" Configuració bàsica de Vim feta per Gianfranco Sáenz {VER 1.0}
" 
" Sentiu-vos lliures d'agafar aquesta config i fer-la servir i modificar com vos roti
" En tot cas, vos recomanaria que tingueu el següent tree de directoris al vostre home, igual continuo ampliant la config base
" per aprofitar-lo millor com ara inserció de colorschemes i tonteries vàries:
" .vim
" ├── autoload
" ├── colors
" └── plugged
" 
" Qualsevol dubte, m'envieu un whatsapp.
" Si teniu curiositat per les comandes o opcions fetes servir en aquest fitxer, podeu fer $ :help <variable | opció>
"" Per evitar conflictes amb Vi, desactivem la compatibilitat implícita:
set nocompatible




""" # CONFIGURACIÓ BÀSICA # 

" Vim pot detectar el tipus de fitxer que estem editant 
filetype on 

" Vim pot mostrar parts del codi en una altre font o color (per exemple, variables / noms de funció / etc)
syntax on

" Evitar fitxers de backup
set nobackup




""" # CONFIGURACIÓ DE LA INTERFICIE DE VIM #

" Aquí seleccionarem si volem fer servir la primera columna de vim per mostrar el número de línia com estàtica o dinàmica
" En cas de no voler cap dels dos, es comenten els dos i ja està

" Number estàtic
set number 

" Number relatiu al punter (dinàmic) ---> amb aquest podem aprofitar els salts de línia que es poden fer amb <num>J|<num>K 
" set relativenumber

" Si voleu que es mostri a quina columna o línia vos trobeu, descomenteu una de les dues o les dues, el nom ja parla per si sol:
" set cursorline
" set cursorcolum

" Ens diu les línies per les quals el cursor de vim no pot tocar mentre fem scroll
set scrolloff=16


" Ens ensenya en quin mode estem: VISUAL | VISUAL-LINE, NORMAL, INSERT
set showmode


""" # EDICIÓ DE VIM #

" Ens diu la quantitat d'espais efectua a l'hora d'indentar tant amb >> com de forma automàtica
set shiftwidth=4

" Ens diu la quantitat de espais que efectua presionar la tecla TAB
set tabstop=4


""" # CERCA AMB VIM #

" Volem que la cerca sigui incremental
set incsearch

" Volem que la cerca sigui case-insensitive
set ignorecase

" Volem que ens mostri els matches que fem
set showmatch

" Volem que ens mostri la cerca en highlight
set hlsearch
