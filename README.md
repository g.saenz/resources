# Resources d'en Gian
![incons](http://cyber.dabamos.de/88x31/undercon2.gif)  
Aquí trobareu essencialment arxius de configuració, algun que altre manual, redireccions a recursos online útils i més coses que aniré recopilant.

## Arxius de configuració

Els arxius de configuración que trobareu a continuació son plantilles bàsiques de diferents aplicacions CLI.
Si es requereix d'algun arxiu de configuració que creieu que pugui aportar, podeu deixar perfectament un issue al repositori.

### Vim

De vim tinc els següents arxius i documents que vos podran ser útils:
- [Config file](vim/config)
